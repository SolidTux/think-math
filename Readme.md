# Think Math Puzzles

## Prerequisites

* Install [Rust](https://rustup.rs)
* To run a solution use `cargo run --release --bin NAME` where `NAME` is the name of the puzzle.

## Puzzles

1. [`table-puzzle`](https://www.youtube.com/watch?v=T29dydI97zY)
