use std::collections::VecDeque;

fn main() {
    let mut valid = Vec::new();
    permutohedron::heap_recursive(&mut (1..=6).collect::<Vec<_>>(), |p| {
        let mut vec = vec![0];
        vec.extend(p.to_vec());
        let mut v = vec.iter().copied().collect::<VecDeque<_>>();
        for _ in 0..7 {
            let x = v.pop_front().unwrap();
            v.push_back(x);
            if v.iter().enumerate().filter(|(a, i)| a == *i).count() >= 2 {
                return;
            }
        }
        valid.push(vec);
    });
    valid.sort();
    for v in valid {
        println!("{:?}", v);
    }
}
